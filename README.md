## Object similarity

isSimilar.js is based on `_.isEqual` from underscore.js.
The only difference is that arrays are treated as sets. This means that:
- Duplicates in arrays are ignored.
- The order of elements within an array is ignored.

Because of these characteristics, the performance of `isSimilar` is worse than
`_.isEqual`. Assuming that the equality testing takes `O(|eq|)` time, then:

- Eliminating duplicates from an array of size `|a|` takes `O(|eq| * |a|^2)`
  time.
- Comparing similarity of two arrays without duplicates, where both arrays have
  the same length `|a|` (if the lengths were different, then they are trivially
  not similar) takes `O(|eq| * |a|^2)` time.


## Example
```
isSimilar([1, 2], [2, 1, 1]);  // === true
```

## Tests
To run the tests, open index.html in a webbrowser.
