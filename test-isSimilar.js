/* globals isSimilar, test, ok */
test('isSimilar', function() {
    'use strict';
    ok(isSimilar([{a: 1}], [{a: 1}]));
    ok(isSimilar([{a: 1, b: 1}], [{b: 1, a: 1}]));
    ok(isSimilar([{a: [{aa:1}], b: [{bb:1}]}], [{b: [{bb:1}], a: [{aa:1}]}]));
    ok(isSimilar([{a: [{aa:1},{aa:1}], b: [{bb:1},{bb:1},{bb:1}]}], [{b: [{bb:1}], a: [{aa:1}]}]));

    ok(isSimilar({a: 1, b: 2}, {a: 1, b: 2}));
    ok(isSimilar({a: 1, b: 2}, {b: 2, a: 1}));
    ok(!isSimilar({a: 1, b: 2}, {b: 2, a: 2}));
    ok(!isSimilar({a: 1, b: 2}, {a: '1', b: 2}));
    ok(isSimilar({a: [1, 2], b: 2}, {a: [1, 2], b: 2}));
    ok(isSimilar({a: [1, 2], b: 2}, {a: [2, 1], b: 2}));
    ok(!isSimilar({a: [1, 2], b: 2}, {a: [2, 1, 3], b: 2}));

    ok(!isSimilar([{shit:1},{}], [{shit:2},{}]));
    ok(isSimilar([{shit:1},{}], [{shit:1},{}]));
});
